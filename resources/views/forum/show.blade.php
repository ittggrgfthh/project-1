@extends('layout.master')

@section('title')
    Halaman Detail Forum {{ $forum->user->name }}
@endsection

@section('content')


<div class="row">
    <div class="col-12">
        <h3>{{ $forum->judul }}</h3>
        <span class="badge badge-info mb-2">{{ $forum->kategori->name }}</span><br>
        <img src="{{ asset('forum-img/' . $forum->image) }}" alt="">
        <div class="card-body">
            <p>{{ $forum->question }}</p>
        </div>
    </div>
</div>

<h3>Komentar</h3>

@foreach ($forum->komentar as $item)
    <div>Id Komentar: {{ $item->id }} | {{ $item->parent_id }}</div> {{-- debug id --}}
    @if ($item->parent_id === null)
        <div class="card bg-secondary">
            <div class="card-header">
                {{ $item->parent_id }} | {{ $item->user->name }}
            </div>
            <div class="card-body">
                <p class="card-text">{{ $item->komentar }}</p>
                    <form action="../komentar/{{ $item->id }}" method="POST">
                        @csrf
                        @method('DELETE')
                        <a class="btn btn-primary" data-toggle="collapse" href="#collapseReply{{ $item->id }}" role="button" aria-expanded="false" aria-controls="collapseReply{{ $item->id }}">
                            Reply
                        </a>
                        <a class="btn btn-warning" data-toggle="collapse" href="#collapseEdit{{ $item->id }}" role="button" aria-expanded="false" aria-controls="collapseEdit{{ $item->id }}">
                            Edit
                        </a>
                        <input type="submit" value="Delete" class="btn btn-danger">
                    </form>
                <form action="../komentar/{{ $item->id }}" method="POST">
                    @csrf
                    @method('put')
                    <div class="collapse mt-2" id="collapseEdit{{ $item->id }}">
                        <textarea name="komentar" id="" class="col" rows="5" placeholder="{{ $item->komentar }}">{{ $item->komentar }}</textarea>
                        <button type="submit" class="btn btn-primary">Kirim</button>
                    </div>
                </form>
                
                <form action="../komentar" method="POST">
                    @csrf
                    <div class="collapse mt-2" id="collapseReply{{ $item->id }}">
                    <input type="hidden" value="{{ $forum->id }}" name="forum_id">
                    <input type="hidden" value="{{ $item->id }}" name="parent_id">
                        <textarea name="komentar" id="" class="col" rows="5" placeholder="Balas..."></textarea>
                        <button type="submit" class="btn btn-primary">Kirim</button>
                    </div>
                </form>
            </div>
        </div>

        @foreach ($forum->komentar as $item2)
        @if ($item->id === $item2->parent_id)
            <div class="card ml-4">
                <div class="card-header">
                    {{ $item2->parent_id }} | {{ $item2->user->name }}
                </div>
                <div class="card-body">
                    <p class="card-text">{{ $item2->komentar }}</p>
                        <form action="../komentar/{{ $item2->id }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <a class="btn btn-primary" data-toggle="collapse" href="#collapseReply{{ $item2->id }}" role="button" aria-expanded="false" aria-controls="collapseReply{{ $item2->id }}">
                                Reply
                            </a>
                            <a class="btn btn-warning" data-toggle="collapse" href="#collapseEdit{{ $item2->id }}" role="button" aria-expanded="false" aria-controls="collapseEdit{{ $item2->id }}">
                                Edit
                            </a>
                            <input type="submit" value="Delete" class="btn btn-danger">
                        </form>
                    <form action="../komentar/{{ $item2->id }}" method="POST">
                        @csrf
                        @method('put')
                        <div class="collapse mt-2" id="collapseEdit{{ $item2->id }}">
                            <textarea name="komentar" id="" class="col" rows="5" placeholder="{{ $item2->komentar }}">{{ $item2->komentar }}</textarea>
                            <button type="submit" class="btn btn-primary">Kirim</button>
                        </div>
                    </form>
                    
                    <form action="../komentar" method="POST">
                        @csrf
                        <div class="collapse mt-2" id="collapseReply{{ $item2->id }}">
                        <input type="hidden" value="{{ $forum->id }}" name="forum_id">
                        <input type="hidden" value="{{ $item2->id }}" name="parent_id">
                            <textarea name="komentar" id="" class="col" rows="5" placeholder="Balas..."></textarea>
                            <button type="submit" class="btn btn-primary">Kirim</button>
                        </div>
                    </form>
                </div>
            </div>
        @else
            @foreach ($forum->komentar as $item3)
            @if ($item2->id === $item3->parent_id)
                <div class="card ml-4">
                    <div class="card-header">
                        {{ $item->parent_id }} | {{ $item->user->name }}
                    </div>
                    <div class="card-body">
                        <p class="card-text">{{ $item->komentar }}</p>
                            <form action="../komentar/{{ $item->id }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <a class="btn btn-primary" data-toggle="collapse" href="#collapseReply{{ $item->id }}" role="button" aria-expanded="false" aria-controls="collapseReply{{ $item->id }}">
                                    Reply
                                </a>
                                <a class="btn btn-warning" data-toggle="collapse" href="#collapseEdit{{ $item->id }}" role="button" aria-expanded="false" aria-controls="collapseEdit{{ $item->id }}">
                                    Edit
                                </a>
                                <input type="submit" value="Delete" class="btn btn-danger">
                            </form>
                        <form action="../komentar/{{ $item->id }}" method="POST">
                            @csrf
                            @method('put')
                            <div class="collapse mt-2" id="collapseEdit{{ $item->id }}">
                                <textarea name="komentar" id="" class="col" rows="5" placeholder="{{ $item->komentar }}">{{ $item->komentar }}</textarea>
                                <button type="submit" class="btn btn-primary">Kirim</button>
                            </div>
                        </form>
                        
                        <form action="../komentar" method="POST">
                            @csrf
                            <div class="collapse mt-2" id="collapseReply{{ $item->id }}">
                            <input type="hidden" value="{{ $forum->id }}" name="forum_id">
                            <input type="hidden" value="{{ $item->id }}" name="parent_id">
                                <textarea name="komentar" id="" class="col" rows="5" placeholder="Balas..."></textarea>
                                <button type="submit" class="btn btn-primary">Kirim</button>
                            </div>
                        </form>
                    </div>
                </div>
            @endif
            @endforeach
        @endif
        @endforeach
    @endif
    
@endforeach

@auth
<form method="POST" action="/komentar">
    @csrf
    <input type="hidden" value="{{ $forum->id }}" name="forum_id">
    <div class="form-group">
      <textarea name="komentar" cols="30" rows="10" class="form-control" placeholder="Isi Komentar..."></textarea>
    </div>
    @error('forum_id')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endauth

<a href="/forum" class="btn btn-secondary mt-4">I'm out, Go back!</a>

@endsection