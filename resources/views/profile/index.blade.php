@extends('layout.master')

@section('title')
    Halaman Update Profile
@endsection

@section('content')
<form method="POST" action="/profile/{{ $profile->id }}">
    @csrf
    @method('PUT')

    <div class="form-group">
        <label>Nama User</label>
        <input type="text" name="umur" class="form-control" value="{{ $profile->user->name }}" disabled>
    </div>
    <div class="form-group">
        <label>Email User</label>
        <input type="text" name="umur" class="form-control" value="{{ $profile->user->email }}" disabled>
    </div>

    <div class="form-group">
      <label>Umur</label>
      <input type="text" name="umur" class="form-control" value="{{ $profile->umur }}">
    </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
      <label for="exampleInputPassword1">Biodata</label>
      <textarea name="bio" rows="10" class="form-control">{{ $profile->bio }}</textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label for="exampleInputPassword1">Alamat</label>
        <textarea name="alamat" rows="10" class="form-control">{{ $profile->alamat }}</textarea>
      </div>
      @error('alamat')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror

    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection