<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Berita;
use File;

class BeritaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }
    
    public function index()
    {
        $berita = Berita::all();

        return view('berita.index', compact('berita'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cast = DB::table('cast')->get();
        return view('berita.create', compact('cast'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required',
            'content' => 'required',
            'cast_id' => 'required',
            'thumbnail' => 'required|image|mimes:jpeg,png,jpg|max:2048'
        ]);

        $thumbnailName = time(). '.' . $request->thumbnail->extension();
        $request->thumbnail->move(public_path('gambar'), $thumbnailName);

        $berita = new Berita;
 
        $berita->judul = $request->judul;
        $berita->content = $request->content;
        $berita->cast_id = $request->cast_id;
        $berita->thumbnail = $thumbnailName;
 
        $berita->save();

        return redirect('berita/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $berita = Berita::findOrFail($id);
        
        return view('berita.show', compact('berita'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cast = DB::table('cast')->get();
        $berita = Berita::findOrFail($id);
        
        return view('berita.edit', compact('berita', 'cast'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'judul' => 'required',
            'content' => 'required',
            'cast_id' => 'required',
            'thumbnail' => 'image|mimes:jpeg,png,jpg|max:2048'
        ]);

        $berita = Berita::find($id);

        if($request->has('thumbnail')){
            $thumbnailName = time(). '.' . $request->thumbnail->extension();

            $request->thumbnail->move(public_path('gambar'), $thumbnailName);
    
            $berita->judul = $request->judul;
            $berita->content = $request->content;
            $berita->cast_id = $request->cast_id;
            $berita->thumbnail = $thumbnailName;
        }
        else{
            $berita->judul = $request->judul;
            $berita->content = $request->content;
            $berita->cast_id = $request->cast_id;
        }

        $berita->update();

        return redirect('/berita');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $berita = Berita::find($id);
 
        $path = 'gambar/';
        File::delete($path . $berita->thumbnail);
        
        $berita->delete();

        return redirect('/berita');
    }
}
