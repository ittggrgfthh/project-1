@extends('layout.master')

@section('title')
    Halaman Create Forum
@endsection

@section('content')
<form method="POST" action="/forum" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label>Judul Pertanyaan</label>
        <input type="text" name="judul" class="form-control">
    </div>
    @error('judul')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Kategori</label>
        <select name="kategori_id" class="form-control" id="">
            <option value="">--- Pilih Kategori ---</option>
            @foreach ($kategori as $item)
                <option value="{{ $item->id }}">{{ $item->name }}</option>
            @endforeach
        </select>
    </div>
    @error('kategori_id')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label for="exampleInputPassword1">Deskripsi Pertanyaan</label>
      <textarea name="question" cols="30" rows="10" class="form-control"></textarea>
    </div>
    @error('question')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label for="exampleInputPassword1">Tambah Gambar</label>
        <input type="file" name="image" class="form-control">
    </div>

    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection