@extends('layout.master')

@section('title')
    Halaman Home
@endsection
    
@section('content')
    <h1>Media Online</h1>
    <h3>Sosial Media Developer</h3>
    <p>Belajar dan berbagi agar hidup menjadi lebih baik</p>
    <h3>Benefit join di Media Online</h3>
    <ul>
        <li>Mendapatkan motivasi dari sesama Developer</li>
        <li>Sharing knowledge</li>
        <li>Dibuat oleh calon web developer terbaik</li>
    </ul>
    <h3>Cara Bergabung ke Media Online</h3>
    <ol>
        <li>Mengunjungi Website ini</li>
        <li>Mendaftarkan di <a href="/biodata">Form Sign Up</a></li>
        <li>Selesai</li>
    </ol>
@endsection