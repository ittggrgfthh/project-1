<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Forum extends Model
{
    protected $table = 'forum';
    protected $fillable = ['question', 'image', 'kategori_id', 'user_id'];

    public function kategori()
    {
        return $this->belongsTo('App\Kategori', 'kategori_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function komentar()
    {
        return $this->hasMany('App\Komentar');
    }
}
