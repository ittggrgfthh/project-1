<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Berita extends Model
{
    protected $table = 'berita';
    protected $fillable = ['judul', 'content', 'cast_id', 'thumbnail'];

    public function cast()
    {
        return $this->belongsTo('App\Cast', 'cast_id');
    }
}
