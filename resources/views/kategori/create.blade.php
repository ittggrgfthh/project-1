@extends('layout.master')

@section('title')
    Halaman Create Kategori
@endsection

@section('content')
<form method="POST" action="/kategori">
    @csrf
    <div class="form-group">
      <label>Nama Kategori</label>
      <input type="text" name="name" class="form-control">
    </div>
    @error('name')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
      <label for="exampleInputPassword1">Deskripsi</label>
      <textarea name="deskripsi" cols="30" rows="10" class="form-control"></textarea>
    </div>
    @error('deskripsi')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection