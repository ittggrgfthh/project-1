<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function biodata(){
        return view('page.register');
    }

    public function send(Request $request){
        $name = $request['first_name'] . " " . $request['last_name'];

        return view('page.welcome', compact('name'));
    }
}
