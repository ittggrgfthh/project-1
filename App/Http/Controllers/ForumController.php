<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Forum;
use DB;
use Illuminate\Support\Facades\Auth;
use File;

class ForumController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    //masih butuh database kategori, belum bisa di compact
    public function index()
    {
        $forum = Forum::all();
        return view('forum.index', compact('forum'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kategori = DB::table('kategori')->get();
        return view('forum.create', compact('kategori'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required',
            'question' => 'required',
            'kategori_id' => 'required',
            'image' => 'image|mimes:jpeg,png,jpg|max:2048'
        ]);

        $imageName = time(). '.' . $request->image->extension();
        $request->image->move(public_path('forum-img'), $imageName);

        $forum = new Forum;

        $forum->judul = $request->judul;
        $forum->question = $request->question;
        $forum->kategori_id = $request->kategori_id;
        $forum->user_id = Auth::id();
        $forum->image = $imageName;
 
        $forum->save();

        return redirect('forum/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $forum = Forum::findOrFail($id);
        
        return view('forum.show', compact('forum'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kategori = DB::table('kategori')->get();
        $forum = Forum::findOrFail($id);
        
        return view('forum.edit', compact('forum', 'kategori'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'judul' => 'required',
            'question' => 'required',
            'kategori_id' => 'required',
            'image' => 'image|mimes:jpeg,png,jpg|max:2048'
        ]);

        $forum = Forum::find($id);

        if($request->has('image')){
            $imageName = time(). '.' . $request->image->extension();

            $request->image->move(public_path('forum-img'), $imageName);
    
            $forum->judul = $request->judul;
            $forum->question = $request->question;
            $forum->kategori_id = $request->kategori_id;
            $forum->image = $imageName;
        }
        else{
            $forum->judul = $request->judul;
            $forum->question = $request->question;
            $forum->kategori_id = $request->kategori_id;
        }

        $forum->update();

        return redirect('/forum');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $forum = Forum::find($id);
 
        $path = 'forum-img/';
        File::delete($path . $forum->image);
        
        $forum->delete();

        return redirect('/forum');
    }
}
