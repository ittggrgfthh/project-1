@extends('layout.master')

@section('title')
    Halaman Edit Berita
@endsection

@section('content')
<form method="POST" action="/berita/{{ $berita->id }}" enctype="multipart/form-data">
    @csrf
    @method('PUT')
    <div class="form-group">
      <label>Judul Berita</label>
      <input type="text" value="{{ $berita->judul }}" name="judul" class="form-control">
    </div>
    @error('judul')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label for="exampleInputPassword1">Content</label>
        <textarea name="content" cols="30" rows="10" class="form-control">{{ $berita->content }}</textarea>
    </div>
    @error('content')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label for="exampleInputPassword1">Cast</label>
        <select name="cast_id" class="form-control" id="">
            <option value="">--- Pilih Cast ---</option>
            @foreach ($cast as $item)
                @if ($item->id === $berita->cast_id)
                    <option value="{{ $item->id }}" selected>{{ $item->nama }}</option>
                @else
                    <option value="{{ $item->id }}">{{ $item->nama }}</option>
                @endif    
            @endforeach
        </select>
    </div>
    @error('cast_id')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <div class="form-group">
        <label for="exampleInputPassword1">Thumbnail</label>
        <input type="file" name="thumbnail" class="form-control">
    </div>
    @error('content')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection
