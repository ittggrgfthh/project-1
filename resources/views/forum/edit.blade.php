@extends('layout.master')

@section('title')
    Halaman Edit Forum
@endsection

@section('content')
<form method="POST" action="/forum/{{ $forum->id }}" enctype="multipart/form-data">
    @csrf
    @method('put')
    <div class="form-group">
        <label>Judul Pertanyaan</label>
        <input type="text" name="judul" value="{{ $forum->judul }}" class="form-control">
    </div>
    @error('judul')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Kategori</label>
        <select name="kategori_id" class="form-control" id="">
            <option value="">--- Pilih Kategori ---</option>
            @foreach ($kategori as $item)
                @if ($item->id === $forum->kategori_id)
                    <option value="{{ $item->id }}" selected>{{ $item->name }}</option>
                @else
                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                @endif               
            @endforeach
        </select>
    </div>
    @error('kategori_id')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label for="exampleInputPassword1">Deskripsi Pertanyaan</label>
      <textarea name="question" cols="30" rows="10" class="form-control">{{ $forum->question }}</textarea>
    </div>
    @error('question')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label for="exampleInputPassword1">Edit Gambar</label>
        <input type="file" name="image" class="form-control">
    </div>

    <a href="/forum" class="btn btn-secondary">Cancel</a>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection