@extends('layout.master')

@section('title')
    Halaman Pendaftaran
@endsection

@section('content')
    <form action="/send" method="POST">
        @csrf
        <div>
            <label>First Name :</label><br><br>
            <input type="text" name="first_name"><br><br>
        </div>
        <div>
            <label>Last Name :</label> <br><br>
            <input type="text" name="last_name"><br><br>
        </div>
        <div>
            <label>Gender</label><br><br>
            <input type="radio" name="gender">
            <label for="html">Male</label><br>
            <input type="radio" name="gender">
            <label for="html">Female</label><br><br>
        </div>
        <div>
            <label>Nationality</label><br><br>
            <select name="nationality">
                <option value="indonesia">Indonesia</option>
                <option value="inggris">Inggris</option>
                <option value="italia">Italia</option>    
            </select><br><br>
        </div>
        <div>
            <label>Language Spoken</label><br><br>
            <input type="checkbox" name="indonesia"> Bahasa Indonesia<br>
            <input type="checkbox" name="english"> English<br>
            <input type="checkbox" name="other"> Other<br><br>
        </div>
        <div>
            <label>Bio</label><br><br>
            <textarea name="message" cols="30" rows="10"></textarea><br>
        </div>

        <input type="submit" value='Sign Up'>
    </form>
@endsection