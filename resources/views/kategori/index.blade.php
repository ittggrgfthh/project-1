@extends('layout.master')

@section('title')
    Halaman Index Kategori
@endsection

@section('content')

<a href="/kategori/create" class="btn btn-primary mb-3">Tambah</a>

<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama Kategori</th>
        <th scope="col">Deskripsi</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        {{-- got data cast from compact at CastController index --}}
        {{-- $key as index, $item as data on $key --}}
        @forelse ($kategori as $key => $item) 
            <tr>
                <td>{{ $key + 1 }}</td>
                <td>{{ $item->name }}</td>
                <td>{{ $item->deskripsi }}</td>
                <td>

                    <form action="/kategori/{{ $item->id }}" method="POST">
                    @csrf
                    @method('delete')
                    <a href="/kategori/{{ $item->id }}" class="btn btn-info btn-sm">Detail</a>
                    <a href="/kategori/{{ $item->id }}/edit" class="btn btn-warning btn-sm">Edit</a>
                    <input type="submit" value="delete" class="btn btn-danger btn-sm">
                    </form>
                </td>
            </tr>
        @empty
            <h1>Data tidak ada</h1>
        @endforelse
    </tbody>
  </table>
@endsection