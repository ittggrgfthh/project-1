<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Komentar extends Model
{
    protected $table = 'komentar';
    protected $fillable = ["komentar", "forum_id", "user_id"];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function forum()
    {
        return $this->belongsTo('App\Forum', 'forum_id');
    }
}
