@extends('layout.master')

@section('title')
    Forum
@endsection

@section('content')
    <div class="col">
        @auth
            <a href="/forum/create" class="btn btn-primary mb-4">Tambah</a>
        @endauth
            @forelse ($forum as $item)
            <div class="row-12">
            <div class="card">
                <h5 class="card-header">{{ $item->user->name }}</h5>
                <div class="card-body">
                    <span class="badge badge-info mb-2">{{ $item->kategori->name }}</span>
                    <a href="/forum/{{ $item->id }}"><p class="card-text mb-4"><b>{{ Str::limit($item->question, 30) }}</b></p></a>
                    @auth
                    <form action="/forum/{{ $item->id }}" method="POST">
                        @csrf
                        @method('DELETE')
                        <a href="/forum/{{ $item->id }}/edit" class="btn btn-warning btn-sm">Edit</a>
                        <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                    </form>    
                @endauth
                </div>
            </div>
            @empty
                
            @endforelse
        </div>
    </div>
@endsection