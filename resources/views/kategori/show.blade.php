@extends('layout.master')

@section('title')
    Halaman Detail Kategori
@endsection

@section('content')
<h1>{{ $kategori->name }}</h1>
<p>{{ $kategori->deskripsi }}</p>

<a href="/kategori" class="btn btn-secondary">I'm out, Go Back!</a>
@endsection