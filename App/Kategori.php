<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    protected $table = "kategori";
    protected $fillable = ["name", "deskripsi"];

    public function forum()
    {
        return $this->hasOne('App\Forum');
    }
}
